package com.quartz.unicorn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuartzUnicornApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuartzUnicornApplication.class, args);
	}
}
