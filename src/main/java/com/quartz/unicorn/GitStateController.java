package com.quartz.unicorn;


import com.quartz.unicorn.domain.GitRepositoryState;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class GitStateController {

    @Inject
    private GitRepositoryState gitRepositoryState;

    @RequestMapping(value = "/gitstate")
    public GitRepositoryState gitState() {
        return gitRepositoryState;
    }
}
