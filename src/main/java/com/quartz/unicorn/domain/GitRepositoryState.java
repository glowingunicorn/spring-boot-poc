package com.quartz.unicorn.domain;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Data
@Component
@PropertySource("classpath:git.properties")
public class GitRepositoryState {

    @Getter
    @Setter
    @Value("${git.commit.user.email}")
    private String gitUserEmail;
    @Getter
    @Setter
    @Value("${git.commit.message.full}")
    private String gitMessageFull;
    @Getter
    @Setter
    @Value("${git.commit.id}")
    private String gitCommitId;
    @Getter
    @Setter
    @Value("${git.branch}")
    private String gitBranch;
    @Getter
    @Setter
    @Value("${git.dirty}")
    private boolean gitDirty;

}
