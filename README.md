# Welcome to the Glowing Unicorn World

# HowTo 

## Build the project


```
#!bash

mvn clean install

```

## run the project on port 8080


```
#!bash

java -jar target/${project}-${version}.jar

```

## To test the git state


```
#!bash

curl -X GET http://localhost:8080/gitstate
```